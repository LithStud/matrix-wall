// @ts-check

class MatrixSymbol {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    render(ctx, x, y) {
        ctx.fillText(
            String.fromCharCode(0x30A0 + Math.floor(Math.random() * 96)),
            x, y);
    }
}

class MatrixSymbolLine {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        /**
         * @type {MatrixSymbol[]}
         */
        this.matrixSymbols = [];
        for (let i = 0; i < 30; i++) {
            this.matrixSymbols.push(new MatrixSymbol(50, i * 10));
        }
    }

    render(ctx) {
        for (let i = 0; i < this.matrixSymbols.length; i++) {
            this.matrixSymbols[i].render(ctx, this.x, this.y - i * 10);
        }
    }
}

/**
 * @type {HTMLCanvasElement}
 * 
 */
// @ts-ignore
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
let width = canvas.width;
let height = canvas.height;
/**
 * @type {MatrixSymbolLine[]}
 */
let matrixSymbols = [];
let matrixSymbolLine = new MatrixSymbolLine(0);
let previousTime = Date.now();
let customFrames = 20;

setup();

function setup() {
    ctx.canvas.height = document.documentElement.clientHeight;
    ctx.canvas.width = document.documentElement.clientWidth;
    for (let i = 0; i < Math.floor(canvas.width / 10); i++) {
        matrixSymbols.push(new MatrixSymbolLine(i * 10, Math.round((Math.random()*200))));
    }

}

function update() {
    matrixSymbols.forEach(line=>{
        line.y += 3;
        if (line.y >= canvas.height + line.matrixSymbols.length*10) {
            line.y = -Math.round((Math.random()*100));
        }
    });
}

function draw() {
    let currentTime = Date.now();
    if (currentTime - previousTime > (1000 / customFrames)) {
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.font = "10px courier";
        ctx.fillStyle = "green";
        update();
        matrixSymbols.forEach(line=>{line.render(ctx)});
        previousTime = currentTime;
    }
    window.requestAnimationFrame(draw);
}

window.requestAnimationFrame(draw);